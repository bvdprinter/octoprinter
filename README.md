# octoprinter

This repository contains everything you need to run [Octoprint](https://github.com/foosel/OctoPrint) in a docker environment.

inspired by [Octoprint docker](https://github.com/OctoPrint/docker)

## Getting started

flash raspbian
secure raspbian
install docker
install samba
run docker container

## secure raspbian

change password
`passwd`

## install docker

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
sudo usermod -a -G docker $USER
sudo reboot
```

## install samba

Installing Samba
`sudo apt-get install samba samba-common-bin`

Configuring Samba
`sudo nano /etc/samba/smb.conf`

Make sure these settings are set in the above file

```bash
workgroup = WORKGROUP
wins support = yes
```

Add this block of code to the very bottom of the smb.conf file

```bash
[watched]
 comment = Watched Folder
 path = /mnt/watched
 browseable = Yes
 writeable = Yes
 only guest = no
 create mask = 0777
 directory mask = 0777
 public = no
```

Run this to add user 'Pi' and create a password
 `sudo smbpasswd -a pi`
